<?php

class Building{

	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	//Name
	public function getName(){
		return $this->name;
	}

	public function setName($name){
			$this->name = $name;
	}
	
	//Floors
	public function getFloors(){
		return $this->floors;
	}

	private function setFloors($floors){
			$this->floors = $floors;
	}

	//Address
	public function getAddress(){
		return $this->address;
	}

	private function setAddress($address){
			$this->address = $address;
	}
	
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');


class Condominium extends Building{
	
}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');